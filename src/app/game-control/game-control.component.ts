import { Component, OnInit, EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'app-game-control',
  templateUrl: './game-control.component.html',
  styleUrls: ['./game-control.component.css']
})
export class GameControlComponent implements OnInit {

  @Output() eventNumber = new EventEmitter<number>();
  curNumber=0;
  interval: any;

  constructor() { }

  ngOnInit() {

  }
  onStartGame() {
    this.interval = setInterval(() => { 
                  this.curNumber = this.curNumber+1;
                  this.eventNumber.emit(this.curNumber);
                }, 1000);
      }   
            

  onStopGame() {
    clearInterval(this.interval);
  }
}
